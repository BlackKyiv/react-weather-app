import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import { useNavigate } from 'react-router-dom';
import React from 'react';

const Login = ({ login }) => {
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = React.useState(false);
  const showErrorMessage = () => {setErrorMessage(true)};

  return (
    <div>
      <ErrorMessage show={errorMessage} />
      <Form
        className="App-Login"
        onSubmit={(e) =>
          handleLoginAttempt(e, navigate, login, showErrorMessage)
        }
      >
        <Form.Group className="App-Login-Group" controlId="loginInput">
          <Form.Label>Login</Form.Label>
          <Form.Control
            type="text"
            name="login"
            placeholder="Enter login"
            required
          />
        </Form.Group>

        <Form.Group className="App-Login-Group" controlId="passInput">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            name="password"
            placeholder="Password"
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Login
        </Button>
      </Form>
    </div>
  );
};

function ErrorMessage({ show }) {
  return (
    <Alert className="delay-alert" hidden={!show} variant="danger">
      <h1>You've entered wrong login or password</h1>
    </Alert>
  );
}

function handleLoginAttempt(e, navigate, login, showErrorMessage) {
  e.preventDefault();
  if (
    e.target.login.value === 'Andrii3' &&
    e.target.password.value === 'Andrii3'
  ) {
    login();
    navigate('/dashboard');
  } else showErrorMessage();
  
}

export default Login;
