import dayjs from 'dayjs';
import React from 'react';
import WeatherCard from './WeatherCard';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

const Dashboard = ({ logout }) => {
  const [loc, setLoc] = React.useState(null);
  React.useEffect(() => getGeoLocation(setLoc), []);
  let pageFilling;
  if (!loc) {
    pageFilling = (
      <div className="spinner-main">
        <Spinner animation="border" role="status" />
      </div>
    );
  } else {
    pageFilling = (
      <main>
        <h1>Weather in {loc.title}...</h1>
        <WeatherGroup woeid={loc.woeid} />
      </main>
    );
  }
  return (
    <div className="dashboard">
      <nav className="nav-bar">
        <Button variant="outline-light" onClick={logout}>
          Logout
        </Button>
        <p>WeatherApp</p>
      </nav>
      {pageFilling}
    </div>
  );
};

function getGeoLocation(setLoc) {
  const geo = navigator.geolocation;
  geo.getCurrentPosition(
    (position) => fetchLocation(position, setLoc),
    () => setLoc({ title: 'Kyiv', woeid: 924938 }),
  );
}

function fetchLocation(position, setLoc) {
  const base_url =
    'https://thingproxy.freeboard.io/fetch/http://metaweather.com/api/location/search';
  const url = `${base_url}/?lattlong=${position.coords.latitude},${position.coords.longitude}`;
  const defaultCity = { title: 'Kyiv', woeid: 924938 };
  fetch(url)
    .then((response) => response.json())
    .then((data) => setLoc(data[0] ?? defaultCity))
    .catch((e) => setLoc(defaultCity));
}

function WeatherGroup({ woeid }) {
  let weatherGroup = [];
  let date = dayjs();
  for (let i = 0; i < 7; i++) {
    weatherGroup.push(<WeatherCard key={i} date={date} woeid={woeid} />);
    date = date.add(1, 'day');
  }

  return <div className="weather-group">{weatherGroup}</div>;
}

export default Dashboard;
