import Spinner from 'react-bootstrap/Spinner';
import React from 'react';
import dayjs from 'dayjs';

const WeatherCard = ({ date, woeid }) => {
  const [weatherData, setWeather] = React.useState(null);
  React.useEffect(() => fetchWeather(date, woeid, setWeather), []);
  if (weatherData) {
    return (
      <div className="weather-card">
        <p>
          <WeatherDate date={date} />
        </p>
        <Image type={weatherData.weather_state_abbr} />
        <Temperature temp={weatherData.max_temp} title="Max: " />
        <Temperature temp={weatherData.min_temp} title="Min: " />
        <Wind
          dir={weatherData.wind_direction_compass}
          speed={weatherData.wind_speed}
        />
        <Data title="Humidity: " value={weatherData.humidity} measureUnit="%" />
        <Data
          title="Pressure: "
          value={weatherData.air_pressure}
          measureUnit=" mbar"
        />
        <Data
          title="Predictability: "
          value={weatherData.predictability}
          measureUnit="%"
        />
      </div>
    );
  } else {
    return <Spinner animation="border" role="status" className="spinner" />;
  }
};

function Wind({ speed, dir }) {
  return (
    <p>
      <b>Wind {dir}: </b>
      {Math.round(speed)}mph
    </p>
  );
}

function WeatherDate({ date }) {
  if (date.format('D/M') === dayjs().format('D/M')) {
    return <b>Today</b>;
  } else if (date.format('D/M') === dayjs().add(1, 'day').format('D/M')) {
    return <b>Tommorow</b>;
  } else {
    return <b>{date.format('D/M')}</b>;
  }
}

function Data({ title, measureUnit, value }) {
  return (
    <p>
      <b>{title}</b>
      {Math.round(value)}
      {measureUnit}
    </p>
  );
}

function Image({ type }) {
  return (
    <img
      width="100"
      src={`https://www.metaweather.com/static/img/weather/${type}.svg`}
      alt={type}
    />
  );
}

function Temperature({ temp, title }) {
  return (
    <p>
      <b>{title}</b>
      {temp > 0 ? '+' + Math.round(temp) : Math.round(temp)} C
    </p>
  );
}

function fetchWeather(date, woeid, setWeather) {
  const proxy = 'https://thingproxy.freeboard.io/fetch/';
  const base_url = 'http://metaweather.com/api/location';
  const url = `${proxy + base_url}/${woeid}/${date.format('YYYY/MM/DD')}`;
  fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data[0]);
      setWeather(data[0]);
    });
}

export default WeatherCard;
