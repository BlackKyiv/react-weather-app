import {
  BrowserRouter,
  Navigate,
  Routes,
  Route,
} from 'react-router-dom';
import React from 'react';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import '../src/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function App() {
  const [authed, setAuthed] = React.useState(false);
  const login = () => {setAuthed(true)};
  const logout = () => {setAuthed(false)};
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login login={login} />} />
        <Route
          path="/dashboard"
          element={
            <PrivateRoute auth={authed}>
              <Dashboard logout={logout} />
            </PrivateRoute>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

function PrivateRoute({ auth, children }) {
  return auth ? children : <Navigate to="/" />;
}
